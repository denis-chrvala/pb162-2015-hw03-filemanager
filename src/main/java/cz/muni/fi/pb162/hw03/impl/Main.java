package cz.muni.fi.pb162.hw03.impl;


import cz.muni.fi.pb162.hw03.FileManager;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main {

    /**
     * Main method of project
     * @param args
     */
    public static void main(String[] args) {

        System.out.println("usage ; checking main arguments");
        if(args.length != 2 ) {
            System.out.println("bad arguments");
            System.exit(1);
        }
        System.out.println(args[1]);

        if (!isFilenameValid(args[0])){
            System.exit(1);
        }

        Logger logger = Logger.getLogger("MyLog");

        FileHandler fh;

        try {
            // This block configure the logger with handler and formatter
            fh = new FileHandler(args[1]);
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            // the following statement is used to log any messages
            logger.info("Hello World");

        } catch (SecurityException e) {
            System.out.println(args[1]);
        } catch (IOException e) {}

        FileManager fm = new FileManagerImpl();

        try {
            fm.executeJob(args[0], args[1]);
        } catch (Exception ex){
            System.out.println(args[1]);
        }
    }

    public static boolean isFilenameValid(String file) {
        File f = new File(file);
        try {
            new FileHandler(file);
            return true;
        }
        catch (IOException e) {
            return false;
        }
    }
}

