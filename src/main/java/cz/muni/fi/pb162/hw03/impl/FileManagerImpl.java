package cz.muni.fi.pb162.hw03.impl;

import cz.muni.fi.pb162.hw03.FileManager;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author Denis (434425)
 * @version 10.1.2016
 */
public class FileManagerImpl implements FileManager {

    JobHandler jh = new JobHandler();
    Logger logger;

    /**
     * Execute job operation specified in {@code jobPath}. Atomicity of job execution is NOT guaranteed. It may fail
     * during the execution. <br/>
     * However all file modifications must be included in log file.
     *
     * @param jobPath path to job file
     * @param logFilePath path where log file should be written
     * @throws Exception Throw appropriate exception in case of error. Feel free to define and throw own exceptions.
     */
    @Override
    public void executeJob(String jobPath, String logFilePath) throws Exception {
        if(jobPath == null) throw new IllegalArgumentException("bad jobPath");
        if(logFilePath == null) throw new IllegalArgumentException("bad logFilePath");

        Path ff = Paths.get(logFilePath);
         //fake logging

        logger = Logger.getLogger("MyLog");

        FileHandler fh;

        try {
            // This block configure the logger with handler and formatter
            fh = new FileHandler(logFilePath);
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            // the following statement is used to log any messages
            logger.info("Hello World");

        } catch (SecurityException e) {
            System.out.println(logFilePath);
        } catch (IOException e) {}



        readJob(jobPath);

        execCopy(jh.getCopyJobs());
        execMove(jh.getMoveJobs());
        execDel(jh.getDeleteJobs());

    }

    /**
     * Provides execution for copy
     * @param jobs
     */
    private void execCopy(List<Job> jobs){
        for (Job j : jobs){
            try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(j.getSrc()))) {
                for (Path filePath : directoryStream) {

                    String file = filePath.toString();
                    String[] split = file.split("\\.");
                    String[] fileS = file.split("\\\\");
                    String filename = fileS[fileS.length - 1];

                    if (split[split.length - 1].equals(j.getFileExten())){
                        try{
                            FileUtils.copyFile(new File(filePath.toString()), new File(j.getDst() + "\\" + filename));
                            logger.log(Level.INFO, "CP;" + j.getSrc() + ";" + j.getDst());
                        }catch(Exception ex){}
                    }
                }
            } catch (Exception ex){}
        }

    }

    /**
     * Provides execution for move
     * @param jobs
     */
    private void execMove(List<Job> jobs){
        for (Job j : jobs){
            try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(j.getSrc()))) {
                for (Path filePath : directoryStream) {

                    String file = filePath.toString();
                    String[] split = file.split("\\.");
                    String[] fileS = file.split("\\\\");
                    String filename = fileS[fileS.length - 1];

                    if (split[split.length - 1].equals(j.getFileExten())){
                        try{
                            FileUtils.moveFile(new File(filePath.toString()), new File(j.getDst() + "\\" + filename));
                            logger.log(Level.INFO,"MV;"+j.getSrc() + ";" + j.getDst());
                        }catch(Exception ex){

                        }
                    }
                }
            } catch (Exception ex){}
        }

    }

    /**
     * Provides execution for delete
     * @param jobs
     */
    private void execDel(List<Job> jobs){
        for (Job j : jobs){
            try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(j.getSrc()))) {
                for (Path filePath : directoryStream) {

                    String file = filePath.toString();
                    String[] split = file.split("\\.");
                    String[] fileS = file.split("\\\\");
                    String filename = fileS[fileS.length - 1];

                    if (split[split.length - 1].equals(j.getFileExten())){
                        try{
                            Files.delete(Paths.get(j.getSrc() + "\\" + filename));
                            logger.log(Level.INFO, "DEL;" + j.getSrc());
                        }catch(Exception ex){}
                    }
                }
            } catch (Exception ex){}
        }

    }

    /**
     * Read instructions from file
     * @param path
     * @throws IOException
     */
    private void readJob(String path) throws IOException {

        List<String> lines = new ArrayList<>();
        String line;

        try (BufferedReader br =  new BufferedReader(new InputStreamReader(new FileInputStream(path)))) {
            while ((line = br.readLine()) != null) {
                if (line.length() > 0) {
                    lines.add(line);
                }
            }
            jh.addLinesToParse(lines);

        }
    }


}
