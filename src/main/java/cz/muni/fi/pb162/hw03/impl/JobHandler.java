package cz.muni.fi.pb162.hw03.impl;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author Denis (434425)
 * @version 10.1.2016
 */
public class JobHandler {

    private List<Job> originalCopyJobs = new ArrayList<>();
    private List<Job> originalMoveJobs = new ArrayList<>();
    private List<Job> originalDeleteJobs = new ArrayList<>();

    private List<Job> copyJobs = new ArrayList<>();
    private List<Job> moveJobs = new ArrayList<>();
    private List<Job> deleteJobs = new ArrayList<>();

    /**
     * Getter to copyJobs
     * @return copyJobs
     */
    public List<Job> getCopyJobs() {
        return copyJobs;
    }

    /**
     * Setter to copyJobs
     * @param  copyJobs
     */
    public void setCopyJobs(List<Job> copyJobs) {
        this.copyJobs = copyJobs;
    }

    /**
     * Getter to moveJobs
     * @return moveJobs
     */
    public List<Job> getMoveJobs() {
        return moveJobs;
    }

    /**
     * Setter to moveJobs
     * @param  moveJobs
     */
    public void setMoveJobs(List<Job> moveJobs) {
        this.moveJobs = moveJobs;
    }

    /**
     * Getter to deleteJobs
     * @return deleteJobs
     */
    public List<Job> getDeleteJobs() {
        return deleteJobs;
    }

    /**
     * Setter to deleteJobs
     * @param  deleteJobs
     */
    public void setDeleteJobs(List<Job> deleteJobs) {
        this.deleteJobs = deleteJobs;
    }

    /**
     * Constructor of JobHandler
     */
    public JobHandler() {
    }

    /**
     * This method parse line of instruction
     * @param lines
     * @return true or false
     */
    public boolean addLinesToParse(List<String> lines){
        String temp;
        String src = "";
        String[] line;

        temp = lines.get(0);
        line = temp.split(";");
        if(!line[0].equals("root")){
                return false;
        }else{
            src = line[1];
        }

        for (int i = 1; i < lines.size(); i++){
            temp = lines.get(i);
            line = temp.split(";");
            if(temp.charAt(0) == '#'){
                continue;
            }

            if (line[0].equals(JobType.CP.name())){
                if (line.length != 3) {
                    continue;
                }
                originalCopyJobs.add(new Job(JobType.CP, line[1], src, line[2]));
            }

            if (line[0].equals(JobType.MV.name())){
                if (line.length != 3) {
                    continue;
                }
                originalMoveJobs.add(new Job(JobType.CP, line[1], src, line[2]));
            }

            if (line[0].equals(JobType.DEL.name())){
                if (line.length != 2) {
                    continue;
                }
                originalDeleteJobs.add(new Job(JobType.CP, line[1], src, ""));
            }

        }

        copyJobs.addAll(originalCopyJobs);
        moveJobs.addAll(originalMoveJobs);
        deleteJobs.addAll(originalDeleteJobs);

        addJobsForSubfolders(src);

        return true;
    }

    /**
     * This line provides recursive call to find all files
     * @param src
     */
    private void addJobsForSubfolders(String src){

        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(src))) {
                for (Path filePath : directoryStream) {


                if (Files.isDirectory(Paths.get(filePath.toString() + "\\"))) {

                    addJobsForSubfolders(filePath.toString());

                    for (Job j : originalCopyJobs) {
                        Job jNew = new Job(j);
                        jNew.setSrc(filePath.toString());
                        copyJobs.add(jNew);
                    }

                    for (Job j : originalMoveJobs) {
                        Job jNew = new Job(j);
                        jNew.setSrc(filePath.toString());
                        moveJobs.add(jNew);
                    }

                    for (Job j : originalDeleteJobs) {
                        Job jNew = new Job(j);
                        jNew.setSrc(filePath.toString());
                        deleteJobs.add(jNew);
                    }

                }
            }
        } catch (Exception ex){
            System.out.println("bad arguments");
        }
    }

}
