package cz.muni.fi.pb162.hw03.impl;

/**
 * @author Denis (434425)
 * @version 10.1.2016
 */
public class Job {

    private JobType jobType;
    private String fileExten;
    private String src;

    /**
     * Getter to dst
     * @return dst
     */
    public String getDst() {
        return dst;
    }

    /**
     * Setter to dst
     * @param  dst
     */
    public void setDst(String dst) {
        this.dst = dst;
    }

    /**
     * Getter to jobType
     * @return jobType
     */
    public JobType getJobType() {
        return jobType;
    }

    /**
     * Setter to jobType
     * @param  jobType
     */
    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

    /**
     * Getter to fileExten
     * @return fileExten
     */
    public String getFileExten() {
        return fileExten;
    }

    /**
     * Setter to fileExten
     * @param  fileExten
     */
    public void setFileExten(String fileExten) {
        this.fileExten = fileExten;
    }

    /**
     * Getter to src
     * @return src
     */
    public String getSrc() {
        return src;
    }

    /**
     * Setter to src
     * @param  src
     */
    public void setSrc(String src) {
        this.src = src;
    }

    private String dst;

    /**
     * Constructor for Job
     * @param jobType
     * @param fileExten
     * @param src
     * @param dst
     */
    public Job(JobType jobType, String fileExten, String src, String dst) {
        this.jobType = jobType;
        this.fileExten = fileExten;
        this.src = src;
        this.dst = dst;
    }

    /**
     * Constructor for Job
     * @param j
     */
    public Job(Job j) {
        this.jobType = j.jobType;
        this.fileExten = new String(j.fileExten);
        this.src = new String(j.src);
        this.dst = new String(j.dst);
    }
}
