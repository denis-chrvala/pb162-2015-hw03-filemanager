package cz.muni.fi.pb162.hw03.impl;

/**
 * @author Denis (434425)
 * @version 10.1.2016
 */
public enum JobType {
    CP,
    MV,
    DEL
};
